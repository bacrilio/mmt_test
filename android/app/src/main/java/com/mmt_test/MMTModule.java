package com.mmt_test;

import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

import java.util.Map;
import java.util.HashMap;

import android.widget.Toast;

import ru.mmt.chatwidget.ChatWidgetInstance;
import ru.mmt.chatwidget.ErrorCode;
import ru.mmt.chatwidget.OnChatLoadListener;

public class MMTModule extends ReactContextBaseJavaModule {

  private static final String CONST1 = "CONST1";

  public MMTModule(ReactApplicationContext reactContext) {
    super(reactContext);
  }

  @Override
  public String getName() {
    return "MMTExample";
  }

  @Override
  public Map<String, Object> getConstants() {
    final Map<String, Object> constants = new HashMap<>();
    constants.put(CONST1, 0);
    return constants;
  }

  @ReactMethod
  public void start() {
      ChatWidgetInstance.getInstance(getReactApplicationContext())
        .start("r8dspmca1mitup5phs13aa2js7", "14091",new OnChatLoadListener() {

          @Override
          public void onSuccess() {
            Toast.makeText(getReactApplicationContext(), "success", Toast.LENGTH_SHORT).show();
          }
          
          @Override
          public void onError(ErrorCode errorCode) {
            Toast.makeText(getReactApplicationContext(), errorCode.toString(), Toast.LENGTH_SHORT).show();
            switch (errorCode) {
              case USER_NOT_LOGGED_IN:
                ChatWidgetInstance.getInstance(getReactApplicationContext()).stop();
            }
          }
        });
  }
}